package com.arrayscollections1.generics.AutoPark;

public abstract class Car implements Comparable<Car> {

  private double maxSpeed;
  private String brand;
  private String model;
  private String country;
  private int year;

  public Car(double maxSpeed, String model, String country, int year) {
    this.maxSpeed = maxSpeed;
    this.model = model;
    this.country = country;
    this.year = year;
  }

  public double getMaxSpeed() {
    return maxSpeed;
  }

  public void setMaxSpeed(double maxSpeed) {
    this.maxSpeed = maxSpeed;
  }

  public String getBrand() {
    return brand;
  }

  protected void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() +
        "{" +
        "maxSpeed=" + maxSpeed +
        ", brand='" + brand + '\'' +
        ", model='" + model + '\'' +
        ", country='" + country + '\'' +
        ", year=" + year +
        '}';
  }


  public int compareTo(Car car1, Car car2) {
    return (int) (car1.getMaxSpeed() - car2.getMaxSpeed());
  }
}
