package com.arrayscollections1.generics.AutoPark;

public class BMW extends Car {

  public BMW(double maxSpeed, String model, String country, int year) {
    super(maxSpeed, model, country, year);
    setBrand("BMW");
  }

  @Override
  public int compareTo(Car o) {
    return (int) o.getMaxSpeed();
  }
}
