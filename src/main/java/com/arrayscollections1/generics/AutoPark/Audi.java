package com.arrayscollections1.generics.AutoPark;

public class Audi extends Car {

  public Audi(double maxSpeed, String model, String country, int year) {
    super(maxSpeed, model, country, year);
    setBrand("Audi");
  }


  @Override
  public int compareTo(Car o) {
    return (int) o.getMaxSpeed();
  }
}
