package com.arrayscollections1.generics.AutoPark;

public class Mercedes extends Car {

  public Mercedes(double maxSpeed, String model, String country, int year) {
    super(maxSpeed, model, country, year);
    setBrand("Mercedes");
  }


  @Override
  public int compareTo(Car o) {
    return (int) o.getMaxSpeed();
  }
}
