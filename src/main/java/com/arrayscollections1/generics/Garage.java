package com.arrayscollections1.generics;

import com.arrayscollections1.generics.AutoPark.Car;

import java.util.*;

public class Garage<T extends Car> {

  private List<T> garage;

  public Garage() {
    garage = new ArrayList<T>();
  }

  public void addAvtoPark(List<T> list) {
    garage.addAll(list);
  }

  public List<? extends Car> getAvtoPart() {
    return garage;
  }

  public void setCar(T car) {
    garage.add(car);
  }

  public void showGarage() {
    for (T t : garage) {
      System.out.println(t.toString());
    }
  }
}
