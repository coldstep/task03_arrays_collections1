package com.arrayscollections1.arrays;

public class TestC {

  public int[] test(int[] mas) {
    int[] result = new int[mas.length];
    result[0] = mas[0];

    int size = 0;

    for (int i = 0; i < mas.length; i++) {
      if (i >= mas.length - 1) {
        result[size++] = mas[i];
      } else {
        if ((mas[i] != mas[i + 1])) {
          result[size++] = mas[i];
        }
      }
    }

    int[] temp = result;
    result = new int[size];
    for (int i = 0; i < result.length; i++) {
      result[i] = temp[i];
    }

    return result;
  }

}
