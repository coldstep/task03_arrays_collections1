package com.arrayscollections1.arrays;

import java.util.Date;

public class TestB {

  public int[] test(int[] mas) {
    mas = sort(mas);
    int[] result = new int[mas.length];
    result[0] = mas[0];

    int size = 0;

    for (int i = 0; i < mas.length; i++) {
      if (i >= mas.length - 2) {
        result[size++] = mas[i];
      } else {
        if ((mas[i] != mas[i + 2])) {
          result[size++] = mas[i];
        }
      }
    }

    int[] temp = result;
    result = new int[size];
    for (int i = 0; i < result.length; i++) {
      result[i] = temp[i];
    }

    return result;
  }

  public int[] sort(int[] mas) {

    int a;
    int temp;

    for (int i = 0; i < mas.length - 1; i++) {
      for (int j = 0; j < mas.length - i - 1; j++) {
        a = mas[j];
        if (a > mas[j + 1]) {
          temp = mas[j + 1];
          mas[j + 1] = a;
          mas[j] = temp;
        }
      }
    }

    return mas;
  }
}
