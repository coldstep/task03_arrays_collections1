package com.arrayscollections1.arrays;

public class TestA {

  public int[] getResultOfTestA(int[] mas1, int[] mas2) {
    int[] temp = new int[mas1.length + mas2.length];
    for (int i = 0, j = 0; i < temp.length; i++) {
      temp[i] = (i < mas1.length) ? mas1[i] : mas2[j++];
    }
    return temp;
  }

  public int[] getResultOfTestB(int[] mas1, int[] mas2) {
    int[] first = new TestA().getUnique(mas1, mas2);
    int[] second = new TestA().getUnique(mas2, mas1);

    return getResultOfTestA(first, second);
  }

  private int[] getUnique(int[] mas1, int[] mas2) {
    int[] unique = new int[0];
    for (int i = 0, d = 0; i < mas1.length; i++) {
      int temp = 0;
      boolean checker = false;
      for (int j = 0; j < mas2.length; j++) {
        if (mas1[i] == mas2[j]) {
          checker = false;
          break;
        } else if (mas1[i] != mas2[j]) {
          temp = mas1[i];
          checker = true;
        }
      }

      if (checker) {
        int[] tmp = unique;
        unique = new int[++d];

        for (int j = 0; j < tmp.length; j++) {
          unique[j] = tmp[j];
        }

        unique[d - 1] = temp;
      }
    }

    return unique;
  }
}
