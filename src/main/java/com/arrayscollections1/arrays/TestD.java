package com.arrayscollections1.arrays;

import java.util.Random;

public class TestD {

  private static int heroPower = 25;


  public void beginOurTrip() {
    //0- magic artifact
    //1 - enemy
    int[][] doors = new int[10][2];

    Random random = new Random();

    for (int i = 0; i < doors.length; i++) {
      doors[i][0] = random.nextInt(2);

      doors[i][1] = (doors[i][0] == 0)
          ? random.nextInt(80 - 10) + 10
          : random.nextInt(100 - 5) + 5;

    }

    System.out.println(
        "In " + howManyDoorsIsDangerous(doors, 0, doors.length - 1)
            + " doors we have dangerous enemy");

    for (int i = 0; i < doors.length; i++) {
      System.out.println("Door number " + (i + 1));
      if (doors[i][0] == 0) {
        heroPower += doors[i][1];
        System.out.println(
            "And here we see magic artifact which give us  " +
                doors[i][1] + "power\n" +
                "    {Power : " + heroPower + "}"
        );

      } else {
        System.out.println(
            "Oh no it's a monster with " +
                doors[i][1] + "power"
        );
        if (heroPower >= doors[i][1]) {
          System.out.println(
              "Yeah we kill this monster !!! \n" +
                  "    {Power : " + heroPower + "}");
        } else {
          System.out.println("You died ((");
          System.exit(0);
        }
      }
    }

  }


  private static int howManyDoorsIsDangerous(int[][] mas, int count, int num) {

    if (num == 0) {
      return ((mas[num][0] == 1) && (heroPower < mas[num][1])) ? count + 1 : count;
    }
    return ((mas[num][0] == 1) && (heroPower < mas[num][1])) ?
        howManyDoorsIsDangerous(mas, count + 1, num - 1)
        : howManyDoorsIsDangerous(mas, count, num - 1);

  }


}
