import com.arrayscollections1.arrays.TestA;
import com.arrayscollections1.arrays.TestB;
import com.arrayscollections1.arrays.TestC;
import com.arrayscollections1.arrays.TestD;
import com.arrayscollections1.generics.AutoPark.Audi;
import com.arrayscollections1.generics.AutoPark.BMW;
import com.arrayscollections1.generics.AutoPark.Car;
import com.arrayscollections1.generics.AutoPark.Mercedes;
import com.arrayscollections1.generics.Garage;
import com.sun.corba.se.impl.orb.ParserTable.TestIORToSocketInfo;


public class Test {

  public static void main(String[] args) {
//        genericTest();
        arrayTest();

  }

  private static void arrayTest() {

//    Task A

    System.out.println("\n Result of array Test\n");
    int[] mas1 = {1, 5, 3, 4, 2, 4, 2};
    int[] mas2 = {5, 6, 7, 8, 9, 2, 10, 12};

    int[] mas3 = new TestA().getResultOfTestA(mas1, mas2);
    System.out.println("Task A (a)\n");
    for (int i : mas3) {
      System.out.printf(i + " ");
    }
    mas3 = new TestA().getResultOfTestB(mas1, mas2);
    System.out.println("\n\n Task A (b)\n");
    for (int i : mas3) {
      System.out.printf(i + " ");
    }
    System.out.println();

//    Task B

    System.out.println("\n\n Task B \n");

    int[] mas4 = {1,1,1, 2, 2, 2, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 3, 3, 4, 4, 4, 2, 2, 2,9};
    for (int i : mas4) {
      System.out.printf(i + " ");
    }
    System.out.println();
    mas4 = new TestB().test(mas4);

    for (int i : mas4) {
      System.out.printf(i + " ");
    }

//    Task C

    System.out.println("\n\n Task C \n");
    int[] mas5 = {1,1,1, 2, 2, 2, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 3, 3, 4, 4, 4, 2, 2, 2,9};
    for (int i : mas5) {
      System.out.printf(i + " ");
    }
    System.out.println();
    mas5 = new TestC().test(mas5);

    for (int i : mas5) {
      System.out.printf(i + " ");
    }

//  Task D
    System.out.println("\n\n Task D \n");

    new TestD().beginOurTrip();


  }


  private static void collectionTest() {

  }

  private static void genericTest() {
    System.out.println("\n Result of generic test \n");
    Car rs6 = new Audi(300, " RS-6 ", "Germany", 2017);
    Car r8 = new Audi(350, " R-8 ", "Germany", 2018);
    Car m5 = new BMW(400, " M5 F90 ", "Germany", 2018);
    Car m3 = new BMW(350, " M3 I350 ", "Germany", 2018);
    Car amg = new Mercedes(340, " C63 ", "Germany", 2018);
    Car amgGt = new Mercedes(390, " Amg GT-R ", "Germany", 2018);

    Garage<Car> carGarage = new Garage();

    Audi rs61 = new Audi(300, " RS-6 ", "Germany", 2017);
    Audi r81 = new Audi(350, " R-8 ", "Germany", 2018);

    carGarage.setCar(rs6);
    carGarage.setCar(r8);
    carGarage.setCar(m5);
    carGarage.setCar(m3);
    carGarage.setCar(amg);
    carGarage.setCar(amgGt);

    carGarage.showGarage();

    Garage<Audi> audiGarage = new Garage();

    audiGarage.setCar(rs61);
    audiGarage.setCar(r81);
    System.out.println("\n");
    audiGarage.showGarage();

    System.out.println();

  }
}
